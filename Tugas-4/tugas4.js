
//soal 1
console.log('LOOPING PERTAMA')
var i=2
while(i<=20){
    console.log(i + " - I love coding")
    i+=2
}

console.log("LOOPING KEDUA")
var i=20
while(i>0){
 console.log(i + " - I will become a frontend developer")  
 i-=2
}


//soal 2
for(var i=1;i<=20;i++){
    if(i%3===0&&i%2===1){
        console.log(i + " - I Love Coding")
    }else if(i%2===1){
        console.log(i + " - Santai")
    }else if(i%2===0){
        console.log(i + " - Berkualitas")
    }
}



//soal 3
var newStr=""
for(var i=0;i<7;i++){
    for(var j=0;j<=i;j++){
        newStr+="#"
    }
    if(i<6){
        newStr+='\n'        
    }
}
console.log(newStr)


var kalimat="saya sangat senang belajar javascript"

//soal 4
var kalimatArray = kalimat.split(" ")
console.log(kalimatArray)


var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];

//soal 5
daftarBuah.sort()
for(i=0;i<daftarBuah.length;i++){
    console.log(daftarBuah[i])
}