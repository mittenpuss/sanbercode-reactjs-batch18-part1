
//soal 1
console.log('\n== soal 1 ==')
//Luas Lingkaran
const luasLingkaran = (r) => {
    let phi = 3.14
    return phi*(r*r)
}

console.log('Luas Lingkaran:')
console.log(luasLingkaran(7))


//Keliling Lingkaran
const kelilingLingkaran = (diameter) =>{
    let phi = 3.14
    return phi*diameter
}

console.log('Keliling Lingkaran:')
console.log(kelilingLingkaran(10))


//soal 2
let kalimat = ""

const firstWord = 'saya'
const secondWord = 'adalah'
const thirdWord = 'seorang'
const fourthWord = 'frontend'
const fifthWord = 'developer'

const completeWord = `${firstWord} ${secondWord} ${thirdWord} ${fourthWord} ${fifthWord}`
console.log('\n== soal 2 ==')
console.log(completeWord)



//soal 3
const newFunction = literal = (firstName, lastName) => {
  return {
    firstName,lastName,
    fullName: ()=> {
      console.log(`${firstName} ${lastName}`)
    }
  }
}

//Driver Code 
console.log('\n== soal 3 ==')
newFunction("William", "Imoh").fullName()


//soal 4
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

const {firstName,lastName,destination,occupation,spell} = newObject
console.log('\n== soal 4 ==')
console.log(firstName,lastName,destination,occupation,spell)


//soal 5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]

console.log('\n== soal 5 ==')
console.log(combined)

