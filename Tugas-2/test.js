

function pairs(ar){
    var newArr=[]
    for(var i=0;i<ar.length;i++){

        if(ar.length-1===i){
            return newArr.length
        }

        if(i%2===0){
            if(ar[i]+1===ar[i+1]||ar[i]-1===ar[i+1]){
                newArr.push([ar[i],ar[i+1]]) 
                i=i+1        
            }
        }else if(i%2===1){
            if(ar[i]-1===ar[i-1]){
                newArr.push([ar[i],ar[i-1]])   
            }
        }
    }
    return newArr.length
   };

   function pairs(arr){
    var count=0;
    for (var i=0; i<arr.length; i+=2)
      if (arr[i]-1==arr[i+1]||arr[i]+1==arr[i+1])
        count++;
    return count;
  };


console.log(pairs([1,2,5,8,-4,-3,7,6,5]))

//    pairs([1,2,5,8,-4,-3,7,6,5]) = 3
// The pairs are selected as follows [(1,2),(5,8),(-4,-3),(7,6),5]
// --the first pair is (1,2) and the numbers in the pair are consecutive; Count = 1
// --the second pair is (5,8) and are not consecutive
// --the third pair is (-4,-3), consecutive. Count = 2
// --the fourth pair is (7,6), also consecutive. Count = 3. 
// --the last digit has no pair, so we ignore.