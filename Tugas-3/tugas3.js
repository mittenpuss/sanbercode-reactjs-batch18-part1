


var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

//soal 1
var kataGabung = kataPertama + " " + kataKedua.toUpperCase().charAt(0) + kataKedua.substr(1,5) + " " + kataKetiga + " " + kataKeempat.toUpperCase()
console.log(kataGabung)


var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

//soal 2
var jumlahTotalKata = parseInt(kataPertama)+parseInt(kataKedua)+parseInt(kataKetiga)+parseInt(kataKeempat)
console.log(jumlahTotalKata)


var kalimat = 'wah javascript itu keren sekali'; 

//soal 3
var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4,14);
var kataKetiga = kalimat.substring(15,18);
var kataKeempat = kalimat.substring(19,24);
var kataKelima = kalimat.substring(25,31);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);


//soal 4
var nilai = 100;
if(nilai>=80&&nilai<=100){
    console.log('A')
}else if(nilai>=70&&nilai<80){
    console.log('B')
}else if(nilai>=60&&nilai<70){
    console.log('C')
}else if(nilai>=50&&nilai<60){
    console.log('D')
}else if(nilai<50&&nilai>=0){
    console.log('E')
}else{
    console.log('Nilai Tidak Valid')
}



var tanggal = 7;
var bulan = 9;
var tahun = 1994;

//soal 5
switch(bulan){
    case 1:
        bulan = "Januari"
        break;
    case 2:
        bulan = "Februari"
        break;
    case 3:
        bulan = "Maret"
        break;
    case 4:
        bulan = "April"
        break;
    case 5:
        bulan = "Mei"
        break;
    case 6:
        bulan = "Juni"
        break;
    case 7:
        bulan = "Juli"
        break;
    case 8:
        bulan = "Agustus"
        break;
    case 9:
        bulan = "September"
        break;
    case 10:
        bulan = "Oktober"
        break;
    case 11:
        bulan = "November"
        break;
    case 12:
        bulan = "Desember"
        break;
    default:
        break;
    }

    if(bulan>12||bulan<1){
        console.log("Data tanggal Salah")
    }else{
        console.log(tanggal + " " + bulan + " " + tahun)
    }
    