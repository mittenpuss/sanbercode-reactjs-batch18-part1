
//soal 1
function halo(){
    return "Halo Sanbers!"
}

console.log(halo())



var num1 = 12
var num2 = 4

//soal 2
function kalikan(num1,num2){
    return num1*num2
}
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali)



//soal 3
function introduce(name,age,address,hobby){
    return "Nama Saya " + name + ", umur saya " + age + " tahun," + " alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!" 
}

var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)


//soal 4
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
function newArray(arr){
    var newArrayDaftarPeserta = []
    newArrayDaftarPeserta.push({
        nama: arr[0],
        jenisKelamin: arr[1],
        hobi: arr[2],
        tahunLahir: arr[3]
    })
    return newArrayDaftarPeserta
}
console.log(newArray(arrayDaftarPeserta))


//soal 5
var buah = [
    {
        nama: 'strawberry',
        warna: 'merah',
        adaBijinya: 'tidak',
        harga: 9000
    },
    {
        nama: 'jeruk',
        warna: 'oranye',
        adaBijinya: 'ada',
        harga: 8000
    },
    {
        nama: 'Semangka',
        warna: 'Hijau & Merah',
        adaBijinya: 'ada',
        harga: 10000
    },
    {
        nama: 'Pisang',
        warna: 'Kuning',
        adaBijinya: 'tidak',
        harga: 5000
    },
]

console.log(buah[0])



//soal 6
var dataFilm = []

function filmList (nama,durasi,genre,tahun){
    dataFilm.push({namaFilm: nama, durasiFilm: durasi,genreFilm: genre, tahunFilm: tahun})
}

filmList('One Piece The Movie','120 Menit','Anime','2000')
filmList('Naruto The Movie','115 Menit','Anime','2005')

console.log(dataFilm)