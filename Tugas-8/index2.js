
//soal 2

var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
var timeAvailable = 10000;

readBooksPromise(timeAvailable, books[0])
.then(function(time) {
    readBooksPromise(time, books[1])
        .then(function(time) {
            readBooksPromise(time, books[2])
            .then(function(time) {
                console.log('Saya sudah membaca semua buku! Ulang baca dari awal lagi ah!')
                readBooksPromise(time, books[0])
                .then(function(time) {
                })
                .catch(function(error) {
                    console.log(`Saya kekurangan waktu ${error}`);
                })
           })
            .catch(function(error) {
                console.log(error);
            })
        })
        .catch(function (error) {
            console.log(error);
        })
})
.catch(function(error) {
    console.log(error);
})